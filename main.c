#include<stdio.h>
#include<stdlib.h>
#include<utmp.h>

#include "runtime_options.h"
#include "dynlibhandle.h"

/**
 *  @brief e_flags	enum
 *  @desc enumerates flags used by program;
 *  last enum value (FLAG_COUNT) is number of flags used;
 *  any flags should be added before FLAG_COUNT
 *  and need to be ordered as corresponding letters in g_flag_list constant
 */
static enum
{
	FLAG_GROUP,
	FLAG_HOST,
	FLAG_COUNT
} e_flags;

/**
 *  @brief g_flag_list	const cstring
 *  @desc all handled options used by the program;
 */
static const char* g_flag_list = "gh";

/**
 *  @brief g_flags	int[]
 *  @desc array storing all flags values
 */
static unsigned g_flags[FLAG_COUNT];

/**
 *  @brief g_lib_name	char*
 *  @desc constant cstring storing library name
 */
static const char *g_lib_name = "libgrouplist.so";

/**
 *  @brief gg_func_name	char*
 *  @desc constant cstring storing function name
 */
static const char *g_func_name = "get_user_groups";

/**
 *  @brief show_user_groups	void
 *  @desc prints all groups that a user is a member of to standard output
 *  @param a_username	char*	username of the user whose groups are to be printed
 */
void show_user_groups(char*);

/**
 *  @brief show_host	void
 *  @desc prints host for a login entry from utmp struct to standard output
 *  @param a_login_record	utmp struct storing login record whose host is to be printed
 */
void show_host(struct utmp*);

/**
 *  @brief show_users	void
 *  prints to a standard output usernames and, if the corresponding flags are set,
 *  host and groups of all currently logged in users
 */
void show_users(void);

/**
 *  @brief *p_get_user_groups	char*
 *  @desc pointer to a function; linked to a function from shared library
 *  @param a_username	char*	username of the user whose groups are to be printed
 *  @return char*	cstring storing formated names of all user's groups
 */
char* (*p_get_user_groups)(char*);

int main(int argc, char **argv)
{
	// function reads options from process arguments and sets corresponding flags
	read_options(argc, argv, g_flag_list, FLAG_COUNT, g_flags);
	void *handle = NULL;
	link_lib(g_lib_name, &handle, g_flags, FLAG_GROUP, FLAG_COUNT);
	if(handle)
	{
		link_func(handle, g_func_name, &p_get_user_groups, g_flags, FLAG_GROUP, FLAG_COUNT);
	}
	show_users();
	if(handle)
	{
		link_lib_close(&handle);
	}
	return 0;
}

void show_user_groups(char* a_username)
{
	char *groups = p_get_user_groups(a_username);
	printf("%s", groups);
	free(groups);
}

void show_host(struct utmp *a_login_record)
{
	printf("(");
	char* host = a_login_record->ut_host;
	printf("%s)", host);
}

void show_users(void)
{
	for(struct utmp *utmp_entry = getutent(); NULL != utmp_entry; utmp_entry = getutent())
	{
		if(USER_PROCESS == utmp_entry->ut_type)
		{
			printf("%s", utmp_entry->ut_user);	//print username
			if(g_flags[FLAG_HOST])				// -h flag
			{
				printf("\t");
				show_host(utmp_entry);
			}
			if(g_flags[FLAG_GROUP])				// -g flag
			{
				printf("\t");
				show_user_groups(utmp_entry->ut_user);
			}
			printf("\n");
		}
	}
}

NAME=main
LDIR=lib
GLIB=grouplist
OLIB=runtime_options
LLIB=dynlibhandle
LINK_GLIB=-I $(LDIR)/$(GLIB)/inc -L$(LDIR)/$(GLIB) -Wl,-rpath,$(LDIR)/$(GLIB) -l$(GLIB)
LINK_OLIB=-L$(LDIR)/$(OLIB) -l$(OLIB) -I $(LDIR)/$(OLIB)/inc
LINK_LLIB=-L$(LDIR)/$(LLIB) -l$(LLIB) -I $(LDIR)/$(LLIB)/inc -ldl

main: libgrouplist.so libruntime_options.a libdynlibhandle.a
	@ gcc $(NAME).c -o $@ $(LINK_GLIB) $(LINK_OLIB) $(LINK_LLIB)

lib$(GLIB).so:
	@ cd $(LDIR)/$(GLIB) && make

lib$(OLIB).a:
	@ cd $(LDIR)/$(OLIB) && make

lib$(LLIB).a:
	@ cd $(LDIR)/$(LLIB) && make

.PHONY: stub

stub: clean_glib stublib libruntime_options.a libdynlibhandle.a
	@ gcc -o $(NAME) $(NAME).c $(LINK_GLIB) $(LINK_OLIB) $(LINK_LLIB)

.PHONY: stublib

stublib:
	@ cd $(LDIR)/$(GLIB) && make stub

.PHONY: nolib

nolib: cleanlib libruntime_options.a libdynlibhandle.a
	@ gcc -o $(NAME) $(NAME).c $(LINK_OLIB) $(LINK_LLIB)

.PHONY: clean

clean: cleanlib
	@ rm -f $(NAME) *.o *.s *.i
	
.PHONY: cleanlib

cleanlib: clean_llib clean_olib clean_glib

.PHONY: clean_llib

clean_llib:
	@ cd $(LDIR)/$(LLIB) && make clean

.PHONY: clean_olib

clean_olib:
	@ cd $(LDIR)/$(OLIB) && make clean

.PHONY: clean_glib

clean_glib:
	@ cd $(LDIR)/$(GLIB) && make clean
